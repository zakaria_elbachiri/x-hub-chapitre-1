# Summary of the Chaptre 1 📃

## What's required 🎯

- Learning Git and GitLab
- Learning Unit testing - Test Driven Development- (TDD)

## The learning journey 📈

### Git & GitLab 🦊

I've learnt the basics and the fundamentals of git, alongside with its most used commands for version controlling, such as [**init**, **commit**, **add**, **checkout**, **switch**, **log**,...]; <br>
and for project remoting, like [**clone**, **remote**, **push**, **pull**, ...]

### Unit testing

For unit testing, i've learned the basics on how to use a well-known package called [Jest](https://jestjs.io/) and tried to apply the TDD process on each test 🔄
