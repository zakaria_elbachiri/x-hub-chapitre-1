const { expect } = require("@jest/globals");
const { getCalledCount, add } = require("./string-calculator");

test("Add two numbers", () => {
  //For empty string
  expect(add("")).toBe(0);

  //For one number
  expect(add("1")).toBe(1);

  //For two numbers with commas
  expect(add("1,2,3,10")).toBe(16);

  //For two numbers with new lines (\n)
  expect(add("1\n23")).toBe(24);

  //For two numbers for differelnt dilimiters
  expect(add("//1\n:;!§/.?,23")).toBe(24);

  //For negative numbers
  expect(() => add("-5,-8,54")).toThrow();

  //Ignore numbers > 1000
  expect(add("2,1001")).toBe(2);
});

test('Shows number of times "add" got called', () => {
  expect(getCalledCount()).toBe(7);
});
