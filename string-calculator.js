class StringCalculator {
  constructor() {
    this.nbOfTimesCalled = 0;
  }

  //Add methode
  Add = (stringNumbers) => {
    this.nbOfTimesCalled++;

    let sum = 0,
      stringArray = stringNumbers.split(/[^-0-9]/g).filter((currentValue) => {
        if (currentValue && Number(currentValue) <= 1000) return currentValue;
      }),
      negativeNumbers = stringArray.filter((value) => Number(value) < 0);

    //Throw error in case of negative numbers
    if (negativeNumbers.length > 0) {
      throw new Error("negatives not allowed:" + negativeNumbers.join(","));
    }

    sum = !stringNumbers
      ? 0
      : stringArray.reduce(
          (prevValue, currentValue) => Number(prevValue) + Number(currentValue),
          0
        );
    return sum;
  };

  //GetCalledCount methode
  GetCalledCount = () => {
    return this.nbOfTimesCalled;
  };
}

//Creating an instance
const stringCalculator = new StringCalculator();

// Calling the function to test the error exception
stringCalculator.Add("-5, -52, 2");

module.exports = {
  add: stringCalculator.Add,
  getCalledCount: stringCalculator.GetCalledCount,
};
